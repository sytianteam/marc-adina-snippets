/* 
INSERT THIS CODE ON ONE OF THE .PHP FILE OF THE PLUGIN 
REFERENCE: https://www.dessol.com/blog/disable-plugin-updates-specific-plugin-wordpress-3-8/
*/
add_filter('site_transient_update_plugins', 'dd_remove_update_nag');
function dd_remove_update_nag($value) {
 unset($value->response[ plugin_basename(__FILE__) ]);
 return $value;
}
