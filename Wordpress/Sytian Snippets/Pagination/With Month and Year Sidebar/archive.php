<?php  
 /**
  * @author Trebor Avena - OJT  01/2018 
  */
?>

<section class="page-background">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="page-content-background media-padding">
					<div class="row">
						<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $countRel = 0;
                        while (have_posts() ) : the_post();?>

                        	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col">
								<div class="galleryholder">
									<?php 
									$gallery = get_field('gallery'); 
									$firstImage = $gallery[0];
									$galleryCurrentRel = "gallery-" . $countRel;
									?>
									
									<?php 
										if( !empty($firstImage)): 
												$image 		= 	$firstImage['sizes']['large'];
												$imageAlt 	=	$firstImage['alt']; 
										else: 
												$image 		= 	'https://dummyimage.com/800x800/efefef/ff5b02.jpg';
												$imageAlt 	=	'Chemsult';
										endif 
									?>
									
									<a data-fancybox-group= <?php echo '"'; echo $galleryCurrentRel; ; echo '"'; ?> class="image-show" href="<?php echo $image; ?>">
										<div class="bgimage" style="background-image: url(<?php echo $image; ?>);"></div>
									</a>			

									<?php 
									if( count($gallery) > 1 ): 
									$count = 1;
									?>
								    <ul>
								        <?php foreach( $gallery as $image ):
								        while($count < count($gallery)){

										$currentImage = $gallery[$count];
								?>

							            <li class="hidden">
											<a data-fancybox-group= <?php echo '"'; echo $galleryCurrentRel; ; echo '"'; ?> class="image-show" href="<?php echo $currentImage['sizes']['large']; ?>">
												<img  src="<?php echo $currentImage['sizes']['large']; ?>" class="img-fluid text-center" alt="<?php echo $currentImage['alt']; ?>" />
											</a>													
							            </li>
										<?php  $count++; } ?>
								        <?php endforeach; ?>
								    </ul>
									<?php endif; ?>
								</div>
								<div class="col-xs-12 gallery-text-style">
									<h4 class="text-center"><strong><?php the_title(); ?> </strong> </h4>

									<h3 class="text-center">
									<?php 

										$terms = wp_get_post_terms($post->ID, 'gallery_category');        
										       
										foreach($terms as $term_single) {
											echo $term_single->name;
											echo " ";
										}

										?>
									</h3>
								</div>
							</div>

                        <?php $countRel++; endwhile;?>

                     	<?php
                        global $wp_rewrite;
                        $paginate_base = get_pagenum_link(1);
                        if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
                            $paginate_format = '';
                            $paginate_base = add_query_arg('paged', '%#%');
                        } else {
                            $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
                                user_trailingslashit('page/%#%/', 'paged');;
                            $paginate_base .= '%_%';
                        }

                        echo '<div class="col-xs-12 text-center pagination-margin">';
                        echo paginate_links( array(
                            'base' => $paginate_base,
                            'format' => $paginate_format,
                            'total' => $wp_query->max_num_pages,
                        
                            'current' => ($paged ? $paged : 1),
                           
                            'prev_text' => '<span><i class="fa fa-angle-left"></i></span>',
								'next_text' => '<span><i class="fa fa-angle-right"></i></span>',
                        ));
                        echo "</div>";
                   	 	?>            
                    </div>	
				</div>			
			</div>
			<?php //START OF SIDEBAR ?>
			<div class="col-xs-12 col-md-3 gallerysidebar">
				<div class="page-content-background">
					<div class="archive-margin">
						<h2 class="text-center">Archive</h2>
							<?php $dateactive= get_the_date('Y'); 

							$args = array(
								'post_type' => 'gallery',
								'order' => 'DESC',
								'posts_per_page'      => -1
							);

							$the_query = new WP_Query( $args );
								if ( $the_query->have_posts() ) {
									$lastYear;
									$thisYear;
									$ifCond=0;
								while ( $the_query->have_posts() ) {
									$the_query->the_post();
									if ($ifCond==0) {
										$lastYear=get_the_date('Y');
										$datenow = get_the_date('Y'); 
									?>
									<?php //START OF DATE ?>
									<div class="dateholder <?php if ($dateactive == $datenow):
									echo 'activeyear'; 
									endif; ?>" >
										<div class="news-archive-dates-year"><h3><a class="yearclick" href="javascript:"><?php echo get_the_date('Y'); ?></a></h3></div>
											<?php		
											$ifCond=1;
											$argsMonth = array(
												'post_type' => 'gallery',
												'order' => 'DESC',
												'year' => $lastYear
											);
											$the_month_query = new WP_Query( $argsMonth );
											if ($the_month_query->have_posts()) {
												$lastMonth;
												$thisMonth;
												$ifCondMonth=0;	
												?>
												<div class="month-toggle">
													<div class="news-archive-dates-month" id="<?php echo $lastYear?>">
														<ul class="list-unstyled">
														<?php			
														while ( $the_month_query->have_posts() ) {
																$the_month_query->the_post();
															if ($ifCondMonth==0) {
																$lastMonth=get_the_date('F'); ?>

																<li><a href= "<?php echo get_site_url(); ?>/gallery/<?php echo get_the_date('Y');?>/<?php echo get_the_date('m'); ?>"><?php echo get_the_date('F'); ?></a></li>	
														<?php
															$ifCondMonth=1;
															}
															else {
																$thisMonth=get_the_date('F');
																if ($thisMonth==$lastMonth) {
																}
																else{ ?>
																<li><a href= "<?php echo get_site_url(); ?>/gallery/<?php echo get_the_date('Y');?>/<?php echo get_the_date('m'); ?>"><?php echo get_the_date('F'); ?></a></li>
																<?php
																}
															}
														}
														?>
														</ul>
													</div>
												</div> 
									</div>
									<?php   //END OF DATE
											} // END OF MONTH QUERY
										}

										else {
											$thisYear=get_the_date('Y');
											if ($thisYear==$lastYear) {

											}
											else{
											?>
											<?php $datenow = get_the_date('Y'); ?>
										<div class="dateholder <?php if ($dateactive == $datenow): echo 'activeyear'; endif; ?>">
											<div class="news-archive-dates-year"><h3><a class="yearclick" href="javascript:"><?php echo get_the_date('Y'); ?></a></h3></div>
												<?php
												$argsMonth = array(
													'post_type' => 'gallery',
													'order' => 'DESC',
													'year' => $thisYear
												);
												$the_month_query = new WP_Query( $argsMonth );
												if ($the_month_query->have_posts()) {
													$lastMonth;
													$thisMonth;
													$ifCondMonth=0;		
												?>
												<div class="month-toggle">
													<div class="news-archive-dates-month" id="<?php echo $thisYear?>">
														<ul class="list-unstyled">
														<?php												
														while ( $the_month_query->have_posts() ) {
																$the_month_query->the_post();
																if ($ifCondMonth==0) {
																$lastMonth=get_the_date('F');
																?>
																<li><a href= "<?php echo get_site_url(); ?>/gallery/<?php echo get_the_date('Y');?>/<?php echo get_the_date('m'); ?>"><?php echo get_the_date('F'); ?></a></li>
																<?php
																$ifCondMonth=1;
																}
																else {
																	$thisMonth=get_the_date('F');
																	if ($thisMonth==$lastMonth) {
																	}
																	else{
																	?>
																	<li><a href= "<?php echo get_site_url(); ?>/gallery/<?php echo get_the_date('Y');?>/<?php echo get_the_date('m'); ?>"><?php echo get_the_date('F'); ?></a></li>
																	<?php
																	}
																}
															}
														?>
														</ul>
													</div>
												</div>
											</div>
												<?php
												}

											}
											$lastYear=get_the_date('Y');
										}

										}
										/* Restore original Post Data */
										wp_reset_postdata();
									} else {
										echo "string";
									}

								?>
					</div>
				</div>
			</div>
			<?php //END OF SIDEBAR ?>

		</div>
	</div>
</section>