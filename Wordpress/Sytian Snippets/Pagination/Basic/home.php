<section class="main-layout inner-page-layout">
	<div class="newslist">
		<div class="container">
			<div class="row">
				<?php if( have_posts() ) : ?>
					<?php while(have_posts()): the_post();?>
							<div class="col-12 col-md-6" data-aos="fade-down" data-aos-duration="800" data-aos-easing="linear">			
								<div class=" details">
									<div class="feature-image-wrapper">
										<a href="<?php the_permalink(); ?>">
											<?php if(has_post_thumbnail()): ?>
												<span class="img-box" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>)">
												</span>
		                              			
		                              		<?php else: ?>
		                              			<span class="img-box" style="background: url(http://via.placeholder.com/1024x768"); ">
												</span>
		                              		
		                              		<?php endif; ?>
		                              	</a>
		                           	</div>
									<div class="title-column">
	                                    <h3 class="post-title text-uppercase">
	                                    	<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
	                                    </h3>
	                                    <span class="post-time">DATE POSTED: <?php the_time('n/j/Y'); ?></span>
	              					</div>
									<div class="excerpt"><?php echo max_character( get_the_excerpt(), 150 ); ?></div>
									<div class="button-holder">
										<a class="btn btn-primary" href="<?php the_permalink(); ?>">Read More</a>
									</div>
								</div>
							</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<nav class="navigation">
					<div class="line">
						<?php pagination_bar(); ?>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section>