![img](https://bytebucket.org/sytianteam/sytian-developers/raw/63699b4568e0b010f7bcc1c4f4d1fb96a7189bfc/logo.jpg)


## Sytian Repository

Put the snippets per folder to include any file associated with it. (e.g. Bootstrap)

	Bootstrap(Main Folder)
	--css(Sub Folder)
	  ->main.css
	--js(Sub Folder)
	  ->main.js
	index.html

Always add readme.md file for every snippet as a guide to the other users. 
If you need to style your readme MarkDown file. See [Link](https://guides.github.com/features/mastering-markdown/) 

### Installation (First Run)

1. Always CLONE the [Sytian Developers Repository](https://bitbucket.org/sytianteam/sytian-developers.git) to your specified local directory.
2. COMMIT changes and add specific messages.
3. PUSH it back to the [Sytian Developers Repository](https://bitbucket.org/sytianteam/sytian-developers.git).
3. Request a PULL REQUEST. 

### Updating Repository

1. Before updating anything on the repository. Please make sure to download the latest copy by using the PULL command.
2. COMMIT changes and add specific messages.
3. PUSH it back to the [Sytian Developers Repository](https://bitbucket.org/sytianteam/sytian-developers.git).
3. Request a PULL REQUEST. 

