## Installation Windows:

1. Install Xampp 7.1.14   
>https://www.apachefriends.org/xampp-files/7.1.14/xampp-win32-7.1.14-0-VC14-installer.exe

2. Install Composer   
>https://getcomposer.org/Composer-Setup.exe

3. Go to your htdocs folder  
>(e.g C:\xampp\htdocs\) 

4. Right Click then "Run Git Bash Here"
>Screenshot: http://prntscr.com/ilqsze

5. type the command below for the first time (Not needed if you already did this before to your PC because we already set it as global)   
>composer global require "laravel/installer"

6. Check if laravel is installed type command below  
>laravel  
>  
>Screenshot: http://prntscr.com/ilqs9i

7. Now we install the project type the command below  "laravel new (project-folder-name)"
>laravel new laravel-training  
>  
>Screenshot: http://prntscr.com/ilra83  
>Screenshot: http://prntscr.com/ilrafe

8. Once laravel project is installed go to the project folder to do this see screenshot below  
>Screenshot: http://prntscr.com/im72sy

9. While on the project folder type the command below  
>composer install  
>  
>Screenshot: http://prntscr.com/im72x1

10. Now you can access the public folder to see the default page of laravel  
>http://localhost/laravel-training/public

11. Now you are done installing the default project files of laravel.  
>Screenshot: http://prntscr.com/im71sf
